# Test de postulación para Mundo Pacífico

**Instrucciones de instalación**


1. Crear una base de datos con el nombre "test" dentro de algún gestor de base de datos (phpmyadmin de preferencia).
2. Abrir una terminal dentro de la carpeta del proyecto y ejecutar el comando `php artisan migrate -seed`.
3. Ejecutar en una terminal el comando `npm install` para instalar todas las dependencias.
4. Ejecutar en la terminal el comando `composer update`.
5. Ejecutar en una terminal el comando `php artisan serve`.
6. Ejecutar en otra terminal el comando `npm run watch`.


