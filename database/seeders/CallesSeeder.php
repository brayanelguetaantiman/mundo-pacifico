<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CallesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calles')->insert([
            [
                'calle' => 'El ROBLE 1233',
                'ciudadId' => '1'
            ],
            [
                'calle' => 'MAIPON 2253',
                'ciudadId' => '1'
            ],
            [
                'calle' => 'ANGAMOS 233',
                'ciudadId' => '2'
            ],
            [
                'calle' => 'ESMERALDA 1122',
                'ciudadId' => '2'
            ],
            [
                'calle' => 'MAIPU 233',
                'ciudadId' => '3'
            ],
            [
                'calle' => 'COCHRANE 1112',
                'ciudadId' => '3'
            ],
            [
                'calle' => 'BAHAMONDES 2233',
                'ciudadId' => '4'
            ],
            [
                'calle' => 'HEROES DE LA CONCEPCIÓN 112',
                'ciudadId' => '4'
            ],
            [
                'calle' => 'FRANCISCO BILBAO 4445',
                'ciudadId' => '5'
            ],
            [
                'calle' => 'SINFORIANO OSSA 1121',
                'ciudadId' => '5'
            ],
            [
                'calle' => 'SARGENTO ALDEA 533',
                'ciudadId' => '6'
            ],
            [
                'calle' => 'CAUPOLICÁN 233',
                'ciudadId' => '6'
            ],
            [
                'calle' => 'LIENTUR 2353',
                'ciudadId' => '7'
            ],
            [
                'calle' => 'ARGENTINA 003',
                'ciudadId' => '7'
            ],
            [
                'calle' => 'LAS DELICIAS 1222',
                'ciudadId' => '8'
            ],
            [
                'calle' => 'ZAÑARTU 233',
                'ciudadId' => '8'
            ],
            [
                'calle' => 'LOS CARRERA 1235',
                'ciudadId' => '9'
            ],
            [
                'calle' => 'FREIRE 2392',
                'ciudadId' => '9'
            ],
            [
                'calle' => 'LOS PIQUES 233',
                'ciudadId' => '10'
            ],
            [
                'calle' => 'MANUEL MONTT 1853',
                'ciudadId' => '10'
            ],
            [
                'calle' => 'SAAVEDRA 422',
                'ciudadId' => '11'
            ],
            [
                'calle' => 'ESMERALDA 557',
                'ciudadId' => '11'
            ],
            [
                'calle' => 'LATORRE 965',
                'ciudadId' => '12'
            ],
            [
                'calle' => 'ANDRÉS BELLO 233',
                'ciudadId' => '12'
            ],
            [
                'calle' => 'BERNARDO O HIGGINGS 1967',
                'ciudadId' => '13'
            ],
            [
                'calle' => 'RANCAGUA 233',
                'ciudadId' => '13'
            ],
            [
                'calle' => 'BULNES 88',
                'ciudadId' => '14'
            ],
            [
                'calle' => 'ALCÁZAR 233',
                'ciudadId' => '14'
            ],
            [
                'calle' => 'FRANCISCO SALAZAR 233',
                'ciudadId' => '15'
            ],
            [
                'calle' => 'CANDELARIA 273',
                'ciudadId' => '15'
            ],
            [
                'calle' => 'PEDRO DE VALDIVIA 1163',
                'ciudadId' => '16'
            ],
            [
                'calle' => 'PEDRO MONTT 273',
                'ciudadId' => '16'
            ]



        ]);
    }
}
