<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiudadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('ciudades')->insert([
            [
                'ciudad' => 'CHILLÁN',
                'provinciaId' => '1'
            ],
            [
                'ciudad' => 'YUNGAY',
                'provinciaId' => '1'
            ],
            [
                'ciudad' => 'QUIRIHUE',
                'provinciaId' => '2'
            ],
            [
                'ciudad' => 'COELEMU',
                'provinciaId' => '2'
            ],
            [
                'ciudad' => 'SAN CARLOS',
                'provinciaId' => '3'
            ],
            [
                'ciudad' => 'COIHUECO',
                'provinciaId' => '3'
            ],
            [
                'ciudad' => 'LOS ANGELES',
                'provinciaId' => '4'
            ],
            [
                'ciudad' => 'CABRERO',
                'provinciaId' => '4'
            ],
            [
                'ciudad' => 'CONCEPCION',
                'provinciaId' => '5'
            ],
            [
                'ciudad' => 'CORONEL',
                'provinciaId' => '5'
            ],
            [
                'ciudad' => 'CAÑETE',
                'provinciaId' => '6'
            ],
            [
                'ciudad' => 'LEBU',
                'provinciaId' => '6'
            ],
            [
                'ciudad' => 'ANGOL',
                'provinciaId' => '7'
            ],
            [
                'ciudad' => 'COLLIPULLI',
                'provinciaId' => '7'
            ],
            [
                'ciudad' => 'TEMUCO',
                'provinciaId' => '8'
            ],
            [
                'ciudad' => 'VILLARRICA',
                'provinciaId' => '8'
            ]

        ]);
    }
}
