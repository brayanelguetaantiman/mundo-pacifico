<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProvinciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('provincias')->insert([
            [
                'provincia' => 'DIGUILLIN',
                'regionId' => '1'
            ],
            [
                'provincia' => 'ITATA',
                'regionId' => '1'
            ],
            [
                'provincia' => 'PUNILLA',
                'regionId' => '1'
            ],
            [
                'provincia' => 'BIOBÍO',
                'regionId' => '2'
            ],
            [
                'provincia' => 'CONCEPCIÓN',
                'regionId' => '2'
            ],
            [
                'provincia' => 'ARAUCO',
                'regionId' => '2'
            ],
            [
                'provincia' => 'MALLECO',
                'regionId' => '3'
            ],
            [
                'provincia' => 'CAUTIN',
                'regionId' => '3'
            ]

        ]);
    }
}
