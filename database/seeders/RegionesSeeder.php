<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regiones')->insert([
        
          
            [
                'region' => 'ÑUBLE'
            ],
            [
                'region' => 'BIOBIO'
            ],
            [
                'region' => 'LA ARAUCANIA'
            ]
            
            ]);
    }
}
