
import React from 'react';
import ReactDOM from 'react-dom';
import Select from './Select'



export default function Main() {

    return (
        <div className="card col-8 shadow rounded p-5">
                <h1 className="border-bottom border-4 border-info mb-4 display-6">Test</h1>
                <Select />

        </div>
    );
}



if (document.getElementById('main')) {
    ReactDOM.render(<Main />, document.getElementById('main'));
}
