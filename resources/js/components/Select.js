import React, { useState, useEffect } from 'react';
import axios from 'axios'

export default function Select (){

    const[regiones,setRegiones] = useState([])
    const[provincias,setProvincias] = useState([])
    const[ciudades, setCiudades] = useState([])
    const[calles, setCalles] = useState([])



    const getRegiones = () => {
        axios.get('http://127.0.0.1:8000/api/regiones')
        .then(response => {
            setRegiones(response.data)
        }).catch( error => {
            console.log(error);
        })
    
    }

  const handlerCargarProvincias = ({target:{value}})=>{
    const opcion = Number(value);
    let data = axios.get(`http://127.0.0.1:8000/api/provincias/${opcion}`)
    .then(response => {
    setProvincias(response.data)
    }).catch( error => {
        console.log(error);
    })
    return data

  }

   
  const handlerCargarCiudades = ({target:{value}})=>{
    const opcion = Number(value);
    let data = axios.get(`http://127.0.0.1:8000/api/ciudades/${opcion}`)
    .then(response => {
        setCiudades(response.data)
    }).catch( error => {
        console.log(error);
    })  
    return data

  }
  const handlerCargarCalles = ({target:{value}})=>{
    const opcion = Number(value);
    let data = axios.get(`http://127.0.0.1:8000/api/calles/${opcion}`)
    .then(response => {
        setCalles(response.data)
    }).catch( error => {
        console.log(error);
    })
    return data
  }


useEffect(getRegiones, [])

     return (
         <div className="row">
            <div className="col-md-6 border-end border-info ">
                <h3 className="lead">Selecciones</h3>
                <select className="form-select my-2" onChange={handlerCargarProvincias}>
                     <option value={-1}>Seleccione Región</option>
                      {
                        regiones.map(elemento => (
                              <option key={elemento.id} value={elemento.id}>{ elemento.region}</option>
                           ) )
                      }               
                 </select>

                 <select className="form-select my-2" onChange={handlerCargarCiudades}>
                 <option value={-1}>Seleccione Provincia</option>
                      {
                          provincias.map(elemento => (
                              <option key={elemento.id} value={elemento.id}>{ elemento.provincia}</option>
                           ))
                      }               
                 </select>

                 <select className="form-select my-2" onChange={handlerCargarCalles}>
                 <option value={-1}>Seleccione Ciudad</option>
                      {
                          ciudades.map(elemento => (
                              <option key={elemento.id} value={elemento.id} >{ elemento.ciudad}</option>
                           ) )
                      }               
                 </select>
            </div>
                 
                <div className="col-md-6 ps-4">

                    <h3 className="lead">Calles</h3>
                    <ul className="list-group">

                        {
                            calles.map(elemento => (
                                <li className="list-group-item list-group-item-action" key={elemento.id}>{elemento.calle}</li>
                            ))
                            
                        }
                    </ul>
                </div>
         </div>
     )
     
 
}

