<?php
use App\Http\Controllers\API\RegionController;
use App\Http\Controllers\API\ProvinciaController;
use App\Http\Controllers\API\CiudadController;
use App\Http\Controllers\API\CalleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


 Route::get('regiones', [RegionController::class, 'index']);
 Route::get('provincias/{regionId}', [ProvinciaController::class, 'index']);
 Route::get('ciudades/{provinciaId}', [CiudadController::class, 'index']);
 Route::get('calles/{ciudadId}', [CalleController::class, 'index']);

